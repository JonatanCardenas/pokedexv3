import { ThemeConfig, Row } from 'antd';


export const lightTheme: ThemeConfig = { 
   token:{
       /* colorBgBase:'#d7705f', */
       
       colorTextDescription:'#ef8200',
       colorText: '#2e2e2e',
       colorIcon: "#fff"
    }, 
    components:{
        Drawer:{
            colorBgElevated:'#F5d994',
            colorBgBase:'#d7705f',
        },
        Button:{
          colorText:'#2e2e2e'
         

          
       },
       Card: {
        colorBorderSecondary:'#fff'
      },
      Layout:{
        colorBgBody:'#fff',
        colorText:'#2e2e2e',
        colorTextDescription:'#2e2e2e'
      },
      FloatButton:{
        colorBgElevated:'black',
      },
      Collapse:{
        colorBgContainer: '#F7F7F7'
      }
    }
}
