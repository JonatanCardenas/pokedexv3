import { pokeApi } from "@/api";
import { Pokemon, PokemonGQLResponse, PokemonList, PokemonStats } from "@/interfaces";
import axios from "axios";


export const getPokemonInfo = async (nameOrId: string) => {

  try {
    const { data } = await pokeApi.get<Pokemon>(`/pokemon/${nameOrId}`);
    console.log({ data })

    return {
      id: data.id,
      name: data.name,
      sprites: data.sprites,
      types: data.types,
      stats: data.stats,
      height: data.height,
      weight: data.weight,
      catch: false

    }
  } catch (error) {
    return null;
  }

}

export const getPokemonInfov2 = async (id: string) => {
  const endpoint = "https://beta.pokeapi.co/graphql/v1beta";
  const headers = {
    "content-type": "application/json",
    "Authorization": "<token>"
  };
  const graphqlQuery = {
    "operationName": "MyQuery",
    "query": `query MyQuery {
        pokemon_v2_pokemon(where: {id: {_eq: ${id}}}) {
          id
          name,
          height,
          weight
          pokemon_v2_encounters(distinct_on: location_area_id) {
            pokemon_v2_locationarea {
              pokemon_v2_location {
                pokemon_v2_region {
                  name
                  id
                }
              }
            }
            location_area_id
          }
          pokemon_v2_pokemonstats {
            pokemon_v2_stat {
              name
            }
            base_stat
          }
          pokemon_v2_pokemontypes {
            pokemon_v2_type {
              name
            }
          }
        }
      }
      `,
    "variables": {}
  };

  const response: PokemonGQLResponse = await axios({
    url: endpoint,
    method: 'post',
    timeout: 100000,
    headers: headers,
    data: graphqlQuery
  }).then(({ data }) => {
    return data

  })
  console.log({ response });
  response.data.pokemon_v2_pokemon[0]

  const pokemon: PokemonList = {

    id: response.data.pokemon_v2_pokemon[0].id,
    name: response.data.pokemon_v2_pokemon[0].name,
    img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${response.data.pokemon_v2_pokemon[0].id}.svg`,
    types: response.data.pokemon_v2_pokemon[0].pokemon_v2_pokemontypes.map((tipos) => (tipos.pokemon_v2_type.name.toString())),
    regions: response.data.pokemon_v2_pokemon[0].pokemon_v2_encounters.map((regiones) => (regiones.pokemon_v2_locationarea.pokemon_v2_location.pokemon_v2_region.name)),
    stats: response.data.pokemon_v2_pokemon[0].pokemon_v2_pokemonstats.map<PokemonStats>((stats) => ({ stat: stats.pokemon_v2_stat.name, base: stats.base_stat })),
    height: response.data.pokemon_v2_pokemon[0].height,
    weight: response.data.pokemon_v2_pokemon[0].weight,
    catch: false
  }

  return {
    pokemon
  }
}