import { darkTheme } from "@/themes";
import { ThemeConfig } from "antd";
import { pokemonStatEnum } from "./pokemonStatEnum";

type localThemeCurrente = {
    darkTheme: boolean
}
const getLocalTheme = (): localThemeCurrente => {
    const themeStorage = localStorage.getItem('defaultTheme');
    const objeto = { 'darkTheme': false }
    if (themeStorage == null) return objeto;
    return JSON.parse(themeStorage!)
}
const setLocalTheme = (darkTheme: boolean) => {
    localStorage.removeItem("defaultTheme");
    localStorage.setItem('defaultTheme', JSON.stringify(darkTheme === true ? { 'darkTheme': true } : { 'darkTheme': false }));
}
const onColor = (stat: string) => {
    return pokemonStatEnum[stat as keyof typeof pokemonStatEnum]
}
export default {
    getLocalTheme,
    setLocalTheme,
    onColor
}