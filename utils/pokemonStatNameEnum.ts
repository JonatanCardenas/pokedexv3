export enum pokemonStatNameEnum {
    'hp' = 'HP',
    'attack' = 'ATK',
    'defense' = 'DEF',
    'special-attack' = 'SPA',
    'special-defense' = 'SPD',
    'speed' = 'S',
}