import { Pokemon, PokemonList } from "@/interfaces";
import { pokemonStatNameEnum } from "./pokemonStatNameEnum";

const getLocalStorage = (itemLocalStorage: string): PokemonList[] => {
    const pokemonCatch = localStorage.getItem(itemLocalStorage);
    return JSON.parse(pokemonCatch!)
}

const catchNow = (pokemon: PokemonList): boolean => {
    let catpureNow = getLocalStorage('pokemonCatch');
    if (!catpureNow || catpureNow == null) return false;
    var elementOn = catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id);
    if (elementOn == null) return false;
    return catpureNow.filter((pokeCatch) => pokeCatch.id === pokemon.id)[0]?.catch;
}

const pokemonsCaptured = (): (PokemonList[]) => {
    let catpureNow: PokemonList[] = getLocalStorage('pokemonCatch');
    let pokemonsCaptured: PokemonList[] = [];
    if (catpureNow !== null) {
        pokemonsCaptured = catpureNow.filter((poke) => poke.catch === true);
    }

    return pokemonsCaptured;

}
const getLocalStoragePokemons = (): PokemonList[] => {
    const pokemones = window.localStorage.getItem('pokemons');

    return JSON.parse(pokemones!)
}
const getLocalStoragePokemon = (id: number): PokemonList => {
    const pokemones = getLocalStoragePokemons();
    return pokemones.find(x => x.id === id)!;
}

const setLocalPokemon = (capture: boolean, pokemon: PokemonList) => {
    const pokemonCatch = getLocalStorage('pokemonCatch');
    let pokemonCatchs: PokemonList[] = [];
    let pokemonValue: PokemonList = {
        catch: !capture,
        name: pokemon.name,
        id: pokemon.id,
        regions: pokemon.regions,
        types: pokemon.types,
        img: pokemon.img,
        height: pokemon.height,
        stats: pokemon.stats,
        weight: pokemon.weight
    }
    capture = !capture;
    if (pokemonCatch == null
        || pokemonCatch.length == 0) {

        pokemonCatchs.push(pokemonValue);
        localStorage.setItem('pokemonCatch', JSON.stringify(pokemonCatchs));
    } else {
        localStorage.removeItem("pokemonCatch");
        pokemonCatchs = pokemonCatch.filter((pokeCatch) => pokeCatch.id !== pokemon.id);
        pokemonCatchs.push(pokemonValue);
        localStorage.setItem('pokemonCatch', JSON.stringify(pokemonCatchs));
    }

}
const onStateName = (stat: string) => {
    return pokemonStatNameEnum[stat as keyof typeof pokemonStatNameEnum]
}
const getPokemonListById = (): PokemonList => {
    let id: string = "";


    var input_string = window.location.href;
    id = input_string.substring(input_string.lastIndexOf('/') + 1);
    const pokemon = getLocalStoragePokemon(parseInt(id));
    return pokemon;
}

export default {
    getLocalStorage,
    catchNow,
    pokemonsCaptured,
    getLocalStoragePokemon,
    setLocalPokemon,
    onStateName,
    getPokemonListById
}