export interface PokemonListResponse {
    count: number;
    next?: string;
    previous?: string;
    results: PokemonList[];
}

export interface PokemonList {
    name: string;
    id: number;
    img: string;
    types: string[];
    regions: string[];
    stats: PokemonStats[];
    height: number;
    weight: number;
    catch: boolean;
}

export interface PokemonStats {
    stat: string;
    base: number;
}
