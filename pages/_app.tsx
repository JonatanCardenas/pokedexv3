import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { useEffect, useState } from "react";
import { ConfigProvider, FloatButton } from 'antd';
import { lightTheme, darkTheme } from '@/themes';
import { BackwardOutlined, ForwardOutlined } from '@ant-design/icons';
import localTheme from '@/utils/localTheme';
import Loading from './Loading';
import { EntriesProvider } from '@/context/entries';
function App({ Component, pageProps }: AppProps) {

  const [isDarkMode, setIsDarkMode] = useState(false);
  useEffect(() => {
    const themeCurrent = localTheme.getLocalTheme();
    if (!themeCurrent) return setIsDarkMode(false);

    setIsDarkMode(themeCurrent.darkTheme);
  }, []);
  const handleClick = () => {
    localTheme.setLocalTheme(!isDarkMode);
    setIsDarkMode(!isDarkMode);
  };
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {

    setTimeout(() => {
      setIsLoading(false);
    }, 2500);
  })
  return (
    <div style={{ background: 'black' }}>
      {isLoading === true ?
        <Loading /> :
        <EntriesProvider>
          <ConfigProvider theme={isDarkMode ? darkTheme : lightTheme}>
            <FloatButton
              shape="circle"
              style={{ right: 20 }}
              onClick={handleClick}

              icon={isDarkMode ? <BackwardOutlined style={{ color: 'black' }} /> : <ForwardOutlined style={{ color: 'white' }} />} />
            <Component  {...pageProps}>
            </Component>
          </ConfigProvider>
        </EntriesProvider>

      }
    </div>
  )
}

export default App
