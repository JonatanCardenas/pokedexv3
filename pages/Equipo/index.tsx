import {useEffect, useState} from 'react';
import { LayoutContent } from '@/components/layouts'
import { PokemonList} from '@/interfaces';
import { NoFavorites } from '@/components/ui';
import localPokemon from '@/utils/localPokemon';
import { PokemonListHome } from '@/components/pokemon';


interface Props {
  pokemonsCaptured: PokemonList[];
}

const MyPokemons = () => { 

  const [pokemonsCatch,setPokemons] = useState<PokemonList[]>([]);
  useEffect(() => {
      
    setPokemons(localPokemon.pokemonsCaptured());

  }, [])

console.log('favoritePokemons', pokemonsCatch);
  return (

    <LayoutContent title='Mis Pokémons'>       
      
      {  
        pokemonsCatch.length === 0 ? ( <NoFavorites /> )
        : (
          <PokemonListHome pokemonAll={pokemonsCatch} ></PokemonListHome>
        )        
      }
      
    </LayoutContent>

  )
}

export default MyPokemons;