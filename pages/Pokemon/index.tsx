
import { LayoutContent } from '@/components/layouts'
export default function Home() {
  return (
   <LayoutContent title='Listado de Pokémons'>   
    <h1>Pokedex</h1>
   </LayoutContent>
  )
}
