import { DetalleLayout } from "@/components/layouts";
import { PokemonListHome } from "@/components/pokemon";
import { Pokemon, PokemonCatch, PokemonGQLResponse, PokemonList, PokemonStats } from "@/interfaces";
import { getPokemonInfo, getPokemonInfov2, pokemonStatEnum, pokemonStatNameEnum, pokemonTypesEnum } from "@/utils";
import localPokemon from "@/utils/localPokemon";
import localTheme from "@/utils/localTheme";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Token } from "@mui/icons-material";
import { Button, Card, Col, Divider, Image, Progress, Row, Switch, Tag, Typography, theme } from "antd";
import axios from "axios";
import { GetStaticPaths, GetStaticProps, NextPage } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
const { Title } = Typography;
import { useEffect, useState } from 'react';
const { useToken } = theme;

interface Props {
    pokemon: PokemonList
}
const pokemon2: PokemonList = {
    name: '',
    catch: false,
    height: 12,
    id: 1,
    img: '',
    regions: [],
    stats: [],
    types: [],
    weight: 12
}
const PokemonDetalle: NextPage<Props> = () => {
    const router = useRouter();
    let id: string = "";


    var input_string = window.location.href;
    id = input_string.substring(input_string.lastIndexOf('/') + 1);
    const pokemon = localPokemon.getLocalStoragePokemon(parseInt(id));
    localPokemon.getPokemonListById()
    const [capture, setCapture] = useState(false);
    const tipopokemon: string = pokemon.types[0];
    const colorPokemon: string = pokemonTypesEnum[tipopokemon as keyof typeof pokemonTypesEnum]
    const { token } = useToken();

    useEffect(() => {
        setCapture(localPokemon.catchNow(pokemon))
    }, [])

    return (
        <DetalleLayout title={pokemon.name}>
            <Row gutter={[24, 24]} style={{ marginRight: '0px', background: colorPokemon }} >
                <Col span={12}>
                    <Link href='/' passHref >
                        <Button type="text" shape='circle' icon={<ArrowLeftOutlined style={{ fontWeight: 'bolder', color: token.colorIcon }} />} />
                        <Typography.Text strong style={{ fontWeight: 'bolder', paddingLeft: '10px' }}>Pokedex</Typography.Text >
                    </Link>
                </Col>
                <Col span={12} className='gutter-row' >
                    <div style={{ display: 'flex', justifyContent: 'end', paddingRight: '10px', paddingTop: '5px' }}>
                        <Typography.Text strong style={{ fontWeight: 'bolder' }}>#{pokemon.id < 10 ? `00${pokemon.id}` : pokemon.id < 100 ? `0${pokemon.id}` : pokemon.id}</Typography.Text >
                    </div>
                </Col>
            </Row>
            <Row >
                <Col style={{ display: 'flex', justifyContent: 'center', background: colorPokemon, borderRadius: '0px 0px 40px 40px', width: '100%', maxHeight: '300px', minHeight: '200px' }} >
                    <Image
                        width={200}
                        src={pokemon.img}
                        alt={pokemon.name}
                    />
                </Col>
            </Row>
            <Row justify={'center'}>
                <Title >
                    {pokemon.name}
                </Title>
            </Row>
            <Row justify={'center'}>
                {
                    pokemon.types.map((type) => (
                        <Col key={type} >
                            <Tag color={pokemonTypesEnum[type as keyof typeof pokemonTypesEnum]} bordered style={{ width: '100px', textAlign: 'center', borderRadius: '10px' }}>
                                {type}
                            </Tag>
                        </Col>

                    ))
                }
            </Row>
            <Row>
                <Col span={12}>
                    <Row >
                        <Col span={24} style={{ textAlign: 'center' }}>
                            <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }}  >{pokemon.weight} KG</Typography.Title>
                            <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Weight</Typography.Title>
                        </Col >
                    </Row>
                </Col>
                <Col span={12}>
                    <Row >
                        <Col span={24} style={{ textAlign: 'center' }}>
                            <Typography.Title level={3} style={{ marginBlockEnd: '0em', fontWeight: 'bolder' }} >{pokemon.height}  M</Typography.Title>
                            <Typography.Title level={5} style={{ marginBlockEnd: '0em', marginBlockStart: '0em', color: 'gray' }}>Height</Typography.Title>
                        </Col >
                    </Row>
                </Col>
            </Row>
            <Row style={{ padding: '25px 0px' }}>
                <Col span={24} style={{ textAlign: 'center' }}>
                    <Typography.Title >
                        Base Stats
                    </Typography.Title>
                </Col>
                {
                    pokemon.stats.filter(x => x.stat != 'speed').map((stat) => (
                        <Col span={24} key={stat.stat}>
                            <Row >
                                <Col span={2} style={{ textAlign: 'center' }}>
                                    <Typography.Text >{localPokemon.onStateName(stat.stat)}</Typography.Text>
                                </Col>
                                <Col span={20}>
                                    <Progress percent={stat.base} strokeColor={localTheme.onColor(localPokemon.onStateName(stat.stat))} showInfo={false} strokeWidth={13} />
                                </Col>
                            </Row>
                        </Col>

                    ))}
            </Row>

            <Divider style={{ background: 'white' }} />
            <Row style={{ padding: '10px' }} justify={'end'} >
                <Col span={24} style={{ textAlign: 'center' }} >
                    <Typography.Title style={{ paddingRight: '10px' }} level={4} >CATCH</Typography.Title>
                    <Switch checked={capture} onChange={() => (localPokemon.setLocalPokemon(capture, pokemon), setCapture(!capture))} />
                </Col>
            </Row>

        </DetalleLayout>
    )
}

export default PokemonDetalle;