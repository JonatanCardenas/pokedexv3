import { EntriesState, filter } from "./EntriesProvider"

type UIActionType = | { type: 'UI - Agregar filter', payload: filter[] } | { type: 'UI - Agregar search', payload: string }


export const entriesReducer = (state: EntriesState, action: UIActionType): EntriesState => {

    switch (action.type) {
        case 'UI - Agregar filter':
            return {
                ...state,
                filters: [...action.payload],
            }
        case 'UI - Agregar search':
            return{
                ...state,
                search: action.payload
            }

        default:
            return state;
    }

}