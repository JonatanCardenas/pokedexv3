import { createContext } from "react";
import { filter } from "./EntriesProvider";

interface ContextProps {
    filters: filter[];
    addFilters: (filtro: filter[]) => void;
    search: string;
    addSearch: (search: string) => void;

}


export const EntriesContext = createContext({} as ContextProps);