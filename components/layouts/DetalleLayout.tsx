import { Layout } from 'antd';
import React, { FC, PropsWithChildren } from 'react';
import LayoutDetalleCss from './Layout.module.css';
interface Props extends PropsWithChildren {
    title: string;
}

export const DetalleLayout: FC<Props> = ({ children, title }) => {
    const { Header, Content } = Layout;

    return (
        <Layout title={title} className={LayoutDetalleCss['container-detallelayout']}>

            <title>{title}</title>
            <Content >
                {children}
            </Content>
        </Layout>

    )
}
