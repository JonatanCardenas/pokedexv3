import * as React from 'react';
import { Layout, theme } from 'antd';
import { Navbar } from '../ui';
import { FC, PropsWithChildren } from 'react';

interface Props extends PropsWithChildren { title: string };

export const LayoutContent: FC<Props> = ({ children, title }) => {
  const { Header, Content } = Layout;
  const{useToken}=theme;
  const{token}=useToken()
  return (
    <Layout style={{minHeight: '100vh'}}>  
        <title>{title || "Pokemon App"}</title>
      <Navbar/>
      <Layout>
        <Content>
          {children}
        </Content>
      </Layout>
    </Layout>
  )
};