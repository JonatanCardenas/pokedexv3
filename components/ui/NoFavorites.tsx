import { Row, Typography, Col } from 'antd';
import SinPokemons from '../../utils/images/SinPokemons.png';
import { Link } from '@mui/material';
import Image from 'next/image';

export const NoFavorites= () =>{

    return (
       
        <Col>
            <div style={{width:'100%',display:'flex',justifyContent:'center',height:'100vh',alignItems:'center', opacity: 0.6}}>       
                <Image  src= {SinPokemons} alt='logo pokemón' style={{width:'100%', height:'auto'}}/>
            </div>
        </Col>
      
    )
}




