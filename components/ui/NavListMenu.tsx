import * as React from 'react';
import { ListItemIcon, Box, List, ListItem } from '@mui/material';
import { AreaChartOutlined, GitlabOutlined, SettingOutlined, TeamOutlined, MenuUnfoldOutlined, CloseOutlined } from '@ant-design/icons';
import ListItemButton from '@mui/material/ListItemButton';
import { Col, Row, Button, Drawer, Menu, theme } from 'antd';
import Image from 'next/image';
import style from './Navbar.module.css';
import pokeImage from '../../public/img/pokeapi_256.3fa72200.png';
import pokebola from '../../public/img/pokebola.png';
import Link from 'next/link';
import { ActiveLink } from './ActiveLink';
import { useState } from 'react';

interface Menu {
  menu: string;
  menuIcon: JSX.Element;
  href: string;
}

export default function LayoutMenu() {
  const [collapsed, setCollapsed] = useState(false);
  const [width, setWidth] = useState(310);
  const { useToken } = theme;
  const { token } = useToken()
  const [state, setState] = React.useState({
    left: false,
  });
  const MenuItems: Menu[] = [
    { menu: 'Pokedex', href: './', menuIcon: <GitlabOutlined /> },
    { menu: 'Mi equipo', href: './Equipo', menuIcon: <TeamOutlined /> },
    { menu: 'Estadísticas', href: './Estadisticas', menuIcon: <AreaChartOutlined /> },
    { menu: 'Configuración', href: './Configuracion', menuIcon: <SettingOutlined /> },
    /* {menu:'Acerca de',href:'/AcercaDe',menuIcon: <InfoOutlined />} */
  ];
  const toggleDrawer = (anchor: 'left', open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return;
      }
      setCollapsed(!collapsed);
      setState({ ...state, [anchor]: open });
    };
  const list = (anchor: 'left') => (
    <Box
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Row justify="center" align="top">
        <Col span={12} style={{ textAlign: 'start' }}>
          <Link href="/">
            <Image src={pokebola} alt='logo pokemón' style={{ width: '115px', height: 'auto', margin: '5px' }} />
          </Link>
        </Col>
      </Row>
      <List>
        {MenuItems.map((item, index) => (
          <ListItem key={item.menu} className={style.listemicon}>
            <ListItemButton>
              <ListItemIcon style={{ color: token.colorIcon }}>
                <Link href={item.href} legacyBehavior>
                  {item.menuIcon}
                </Link>
              </ListItemIcon>
              <ActiveLink key={item.href} text={item.menu} href={item.href} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );
  return (
    <div className={style.container}>
      {(['left'] as const).map((anchor) => (
        <React.Fragment key={anchor} >
          <Row justify="end" align="middle" style={{ height: '100%' }}>
            <Col span={12} style={{ paddingLeft: '15px' }}>
              <Button
                size="large"
                onClick={toggleDrawer(anchor, true)}
                type="text"
                style={{ color: token.colorIcon }}
              >
                <MenuUnfoldOutlined className={style.iconmenu} />
              </Button>
            </Col>
            <Col span={12} style={{ display: 'flex', justifyContent: 'end', paddingRight: '15px' }}>
              <Image src={pokeImage} alt='logo pokemón' style={{ width: '115px', height: 'auto' }} />
            </Col>
          </Row>
          <Drawer
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            placement='left'
            width={width}
            closeIcon={<CloseOutlined style={{ color: token.colorIcon }} />}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
