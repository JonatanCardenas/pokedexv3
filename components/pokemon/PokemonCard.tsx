import { PokemonList } from "@/interfaces";
import { pokemonTypesEnum } from "@/utils";
import { Col, Card, Image, Typography } from "antd";
import { useRouter } from "next/router";
import { FC } from "react";

const { Text } = Typography;

interface Props {
  poke: PokemonList,
  paramApp: string;
}

const PokemonCard: FC<Props> = ({ poke, paramApp }) => {
  const router = useRouter();
  const tipopokemon: string = poke.types[0];
  const color: string = pokemonTypesEnum[tipopokemon as keyof typeof pokemonTypesEnum]

  return (
    <>
      <Col xs={12} sm={8} md={6} lg={4} key={poke.id}>

        <Card hoverable 
          style={{
            backgroundColor: `${color}`,
            textAlign: 'center',
            alignItems: 'center', 
            height: '200px'           

          }}
          onClick={() => router.push(paramApp)}
        >
          <Image src={poke.img} width={'100%'} height={100} preview={false} alt={poke.name} />
          <Text style={{
            textTransform: 'capitalize',
            fontSize: '12pt',
            fontWeight: 'bold',
            color: 'white',
            display: "inherit",
            paddingTop: '10px',
            lineHeight: '1.1'

          }}>{poke.name}</Text>

        </Card>
      </Col>
    </>
  )
}

export default PokemonCard