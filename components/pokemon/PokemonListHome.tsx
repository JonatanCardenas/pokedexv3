import { PokemonList } from '@/interfaces';
import { Divider} from "antd";
import { FC, useState } from "react";
import { PokemonFilter, PokemonListBody, PokemonReset, PokemonSearch } from '.';


interface Props {
  pokemonAll: PokemonList[];
}

const PokemonListHome: FC<Props> = ({ pokemonAll }) => {
  const [pokemons, setPokemons] = useState<PokemonList[]>(pokemonAll);
  const [pokemonsSearch, setPokemonsSearch] = useState<PokemonList[]>([]);
  const [pokemonsFilter, setPokemonsFilter] = useState<PokemonList[]>([]);

  return (
    <>

      <div style={{ textAlign: 'center', margin: '10px', alignItems: 'center', display: 'flex', justifyContent: 'center' }}>

        <PokemonSearch pokemonsFilter={pokemonsFilter} pokemonAll={pokemonAll} setPokemons={setPokemons} setPokemonsSearch={setPokemonsSearch} />
        <PokemonFilter pokemonsSearch={pokemonsSearch} pokemonAll={pokemonAll} setPokemons={setPokemons} setPokemonsFilter={setPokemonsFilter} />

      </div>

      <Divider>Lista de Pokemones</Divider>


      <div style={{
        padding: '10px',
        textAlign: 'center'
      }}>

        <PokemonListBody pokemons={pokemons}/>
        <PokemonReset
          pokemonAll={pokemonAll}
          pokemons={pokemons}
          pokemonsFilter={pokemonsFilter}
          pokemonsSearch={pokemonsSearch}
          setPokemons={setPokemons}
          setPokemonsFilter={setPokemonsFilter}
          setPokemonsSearch={setPokemonsSearch}
        
        />
        
      </div>
    </>
  )
}

export default PokemonListHome;