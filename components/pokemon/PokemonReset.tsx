import { EntriesContext } from "@/context/entries";
import { PokemonList } from "@/interfaces"
import { ReloadOutlined } from "@ant-design/icons";
import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";
import { Button } from "antd";
import { Dispatch, FC, SetStateAction, useContext } from 'react';

interface Props{
    pokemons: PokemonList[];
    pokemonsSearch: PokemonList[];
    pokemonsFilter: PokemonList[];
    pokemonAll: PokemonList[];    
    setPokemons : Dispatch<SetStateAction<PokemonList[]>>;
    setPokemonsFilter : Dispatch<SetStateAction<PokemonList[]>>;
    setPokemonsSearch : Dispatch<SetStateAction<PokemonList[]>>;

}

const PokemonReset:FC<Props> = ({
    pokemons,
    pokemonsFilter,
    pokemonsSearch,
    pokemonAll,
    setPokemons,
    setPokemonsFilter,
    setPokemonsSearch
}) => {

    const { addFilters,addSearch} = useContext(EntriesContext)

    let botonRestablecer: ReactJSXElement =<></>

    const reestablecer = () =>{
        setPokemons(pokemonAll);
        setPokemonsFilter([]);
        setPokemonsSearch([]);
        addFilters([]);
        addSearch("");
       
      }

if (pokemons.length === 0 || pokemonsSearch.length > 0  || pokemonsFilter.length > 0  )
  {
    botonRestablecer = (
      <div style={{margin: '30px'}}>
        <Button onClick={reestablecer} style={{
        textAlign: 'center',
        borderRadius: '50px'
        }}>
          
          <ReloadOutlined />  
          
        </Button>
      </div>
    )
  }
  else{
    botonRestablecer = <></>
  }

  return (
    botonRestablecer
  )
}

export default PokemonReset