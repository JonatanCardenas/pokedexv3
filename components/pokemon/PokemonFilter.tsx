import { EntriesContext } from "@/context/entries";
import { PokemonList } from "@/interfaces";
import { pokemonTypesEnum } from "@/utils";
import { FilterOutlined } from "@ant-design/icons"
import { Checkbox, Col, Modal, Row ,Collapse,Button,theme} from 'antd';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import { useState, FC, Dispatch, SetStateAction, useContext } from 'react';

interface Props {
    pokemonsSearch: PokemonList[];
    pokemonAll: PokemonList[];
    setPokemons : Dispatch<SetStateAction<PokemonList[]>>
    setPokemonsFilter : Dispatch<SetStateAction<PokemonList[]>>
}
const { Panel } = Collapse;

type filterCheck = {
  name: string;
  value: boolean;
  filter: string;
}
const {useToken}=theme;



const PokemonFilter:FC<Props> = ({pokemonAll,setPokemons,pokemonsSearch,setPokemonsFilter}) => {
    const {filters, addFilters} = useContext(EntriesContext)
    const pokemonTypes:string[] = Object.keys(pokemonTypesEnum);
    const pokemonRegionAux:string[] = pokemonAll.flatMap( (pokemon) => (pokemon.regions) )
    const pokemonRegion: string[] = pokemonRegionAux.filter((n,i) => pokemonRegionAux.indexOf(n) === i)
    const{token}=useToken();
    

    //Variables Modal
    const [isModalOpen, setIsModalOpen] = useState(false);

    const showModal = () => {
      setIsModalOpen(true);
    };
  
    const handleOk = () => {
      filtrarTipo()
      setIsModalOpen(false);
    };
  
    const handleCancel = () => {
      filtrarTipo()
      setIsModalOpen(false);
    };

    const handleClear = () => {
        addFilters([]);

    };

    // Checks
    const onChangeCheckType = (e: CheckboxChangeEvent) => {
        const type:string = e.target.name? e.target.name: "";        

         var checksAux = [...filters]
         var newfilter:filterCheck = {name: type, value:e.target.checked , filter: 'type'}

        if (e.target.checked)
        {
          checksAux = [...filters,newfilter]
            
        }
        else{
          checksAux = filters.filter(item => item.name !== type)
        }

        addFilters(checksAux)
       
      };

      const onChangeCheckRegion = (e: CheckboxChangeEvent) => {
        const region:string = e.target.name? e.target.name: "";        

        var checksAux = [...filters]
        var newfilter:filterCheck = {name: region, value:e.target.checked , filter: 'region'}

       if (e.target.checked)
       {
         checksAux = [...filters,newfilter]
           
       }
       else{
         checksAux = filters.filter(item => item.name !== region)
       }

       addFilters(checksAux)
      };

      // Collapse

      const onChangeCollapse = (key: string | string[]) => {
        console.log(key);
      };

    //Funciones

    const filtrarTipo = () => 
    {
      const tipoPokemon:string[] = filters.filter(poke => poke.filter === 'type').map( pokemon => pokemon.name);
      const regionPokemon:string[] = filters.filter(poke => poke.filter === 'region').map( pokemon => pokemon.name);

      let pokemonFilter:PokemonList[] = pokemonsSearch.length > 0 ? pokemonsSearch: pokemonAll;

        if(tipoPokemon.length === 0 && regionPokemon.length === 0)
        {
          setPokemons(pokemonFilter);
          setPokemonsFilter([]);
        } 
        else{
           
          if(tipoPokemon.length > 0)
          {            
            pokemonFilter = pokemonFilter.filter(pokemon=> pokemon.types.some(tipo => tipoPokemon.includes(tipo))).map(pokemones => (
                pokemones
            ))
          }

          if(regionPokemon.length > 0)
          {            
            pokemonFilter = pokemonFilter.filter(pokemon=> pokemon.regions.some(region => regionPokemon.includes(region))).map(pokemones => (
                pokemones
            ))
            //pokemonFilter = pokemonFilterRegion;
          }
          
        setPokemons(pokemonFilter);
        setPokemonsFilter(pokemonFilter)
       
        }
        
    }

  return (
    <>
    <Button style={{     
      display: 'contents'
    }}>


    <FilterOutlined 
    
      onClick={() => showModal()}
    style={
      {
        margin: '10px',
        fontSize: '30px',
        
      }
    }/>
    </Button>
    <Modal title="Filtros" open={isModalOpen} onCancel={handleCancel} onOk={handleOk}  closable = {false} 
    footer = {
      [
        <Button key="clear" onClick={handleClear}>
            Limpiar
          </Button>,
          <Button key="accept"  onClick={handleOk}>
            Aceptar
          </Button>,
          
      ]
    }
    >
    <Collapse defaultActiveKey={['1']} onChange={onChangeCollapse} >
      <Panel header="Filtro por tipo" key ="1" style={{background:token.colorBgContainer}}>
        <Row gutter={[8,8]}>

        {pokemonTypes.map((tipo,index) => (
            <Col xs={12} sm={8} md={6} lg={6} key={index}>                
                <Checkbox onChange={onChangeCheckType} name={tipo} key={tipo}  checked={filters?.find(x=> x.name === tipo)?.value}>{tipo}</Checkbox>
            </Col>
        ))}
        </Row>
      </Panel>
      <Panel header="Filtro por región" key="2" style={{background:token.colorBgContainer}}>
        <Row gutter={[8,8]}>

        {pokemonRegion.map((region,index) => (
            <Col xs={12} sm={8} md={6} lg={6} key={index}>                
                <Checkbox onChange={onChangeCheckRegion} name={region} key={region} checked={filters?.find(x=> x.name === region)?.value} >{region}</Checkbox>
            </Col>
        ))}
        </Row>
      </Panel>
      
    </Collapse>
        
      </Modal>
    </>
  )
}

export default PokemonFilter