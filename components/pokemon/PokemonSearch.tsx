import { Input } from "antd"
import { PokemonList } from '@/interfaces';
import { FC, Dispatch, SetStateAction, ChangeEvent, useContext } from 'react';
import { EntriesContext } from "@/context/entries";

const { Search } = Input;

interface Props {
  pokemonsFilter: PokemonList[];
  pokemonAll: PokemonList[];
  setPokemons: Dispatch<SetStateAction<PokemonList[]>>
  setPokemonsSearch: Dispatch<SetStateAction<PokemonList[]>>
}

const PokemonSearch: FC<Props> = ({ pokemonsFilter, pokemonAll, setPokemons, setPokemonsSearch }) => {

  const { search, addSearch } = useContext(EntriesContext)

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {



    addSearch(event.target.value)



  }



  const onSearch = (value: string) => {



    const busqueda: string = value.trim().toLowerCase()

    let pokemonSearch: PokemonList[] = pokemonsFilter.length > 0 ? pokemonsFilter : pokemonAll;





    if (busqueda === "") {

      setPokemons(pokemonSearch)

      setPokemonsSearch([])

    }

    else {

      const searchPokemon = pokemonSearch.filter(poke => poke.name.includes(busqueda))

      setPokemons(searchPokemon)

      setPokemonsSearch(searchPokemon)

    }



  };

  return (
    <Search placeholder="Ingrese el nombre del pokémon" onChange={onChange} value={search} allowClear onSearch={onSearch} size="large" style={{
      width: '100%',
      maxWidth: '350px',
      height: '40px'
    }} enterKeyHint="enter" />

  )
}

export default PokemonSearch