import { PokemonList } from "@/interfaces"
import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";
import { Row } from "antd";
import { FC } from 'react';
import PokemonCard from "./PokemonCard";

interface Props{
    pokemons: PokemonList[];
}

const PokemonListBody:FC<Props> = ({pokemons}) => {
  let listadoPokemon:ReactJSXElement = <></>;

  if(pokemons.length > 0 )
  {
    listadoPokemon = (
      <div>
        <Row gutter={[8, 8]} >
        {      
            pokemons?.map((pokemon) =>
              <PokemonCard poke={pokemon} key={pokemon.id} paramApp={`/Pokemon/${pokemon.id}`} />
            )
          }
        </Row>
      </div>
    )
  }
  else{
    listadoPokemon = (
      <div>
        <p>No se encontraron pokemones...</p>
      </div>)
  }
  return (
   listadoPokemon
  )
}

export default PokemonListBody