
import { precacheAndRoute } from "workbox-precaching";
import { clientsClaim } from 'workbox-core';
import { registerRoute,NavigationRoute} from 'workbox-routing';
import { NetworkOnly,NetworkFirst,StaleWhileRevalidate} from 'workbox-strategies';
import { CacheableResponsePlugin } from 'workbox-cacheable-response/CacheableResponsePlugin';
import { ExpirationPlugin } from 'workbox-expiration/ExpirationPlugin';
import { CacheFirst } from 'workbox-strategies/CacheFirst';

//importScripts('js/sw-db.js');

const STATIC_CACHE    = 'static-caches';
const DYNAMIC_CACHE   = 'dynamic-caches';
const IMAGES_CACHE    = 'images-caches';

precacheAndRoute(self.__WB_MANIFEST);

clientsClaim();

skipWaiting();

/* custom cache rules */


registerRoute(
    // Add in any other file extensions or routing criteria as needed.
    ({ url }) => !(url.origin === self.location.origin) && url.pathname.endsWith('.svg'), // Customize this strategy as needed, e.g., by changing to CacheFirst.
    new StaleWhileRevalidate({
      cacheName: IMAGES_CACHE,
      plugins: [
        // Ensure that once this runtime cache reaches a maximum size the
        // least-recently used images are removed.
        new ExpirationPlugin({ maxEntries: 200 }),
      ],
    })
  );
  
// For images
registerRoute(
    new RegExp('\.(?:png|gif|jpg|jpeg|webp|svg)$'),
    new CacheFirst({
        cacheName: IMAGES_CACHE,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200]
            }),
            new ExpirationPlugin({
                maxEntries: 20,
                maxAgeSeconds: 12 * 60 * 60
            })
        ]
    }), 'GET');

//For JS/CSS
/*
Resources are requested from both the cache and the network in parallel. 
The strategy will respond with the cached version if available, otherwise wait for the network response. 
The cache is updated with the network response with each successful request
*/
registerRoute(
    new RegExp('\.(?:js|css)$'),
    new StaleWhileRevalidate({
        cacheName: DYNAMIC_CACHE,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200]
            }),
            new ExpirationPlugin({
                maxEntries: 20,
                maxAgeSeconds: 12 * 60 * 60
            })
        ]
    })
)

//For HTML
registerRoute(
    new RegExp('/'),
    new StaleWhileRevalidate({
        cacheName: DYNAMIC_CACHE,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200]
            }),
            new ExpirationPlugin({
                maxEntries: 200,
                maxAgeSeconds: 12 * 60 * 60
            })
        ]
    }), 'GET');

registerRoute(
    self.origin,
    new NetworkFirst({
        cacheName: DYNAMIC_CACHE,
        plugins: [
            new CacheableResponsePlugin({
                statuses: [0, 200]
            }),
            new ExpirationPlugin({
                maxEntries: 20,
                maxAgeSeconds: 12 * 60 * 60
            })
        ]
    }), 'GET');

//Other resources
registerRoute(
    new RegExp('/_next/static/'),
    new StaleWhileRevalidate({
        cacheName: STATIC_CACHE,
    })
);


// tareas asíncronas
// self.addEventListener('sync', e => {

//     console.log('SW: Sync');

//     if ( e.tag === 'new-pokemon' ) {

//         // postear a BD cuando hay conexión
//         const respuesta = postPokemon();
        
//         e.waitUntil( respuesta );
//     }

// });