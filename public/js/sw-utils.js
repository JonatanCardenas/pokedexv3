
function apiPokemons(cacheName, req){
    if ( req.clone().method === 'POST' ) {
        // POSTEO de un nuevo mensaje

        if ( self.registration.sync ) {
            return req.clone().text().then( body =>{
    
                // console.log(body);
                const bodyObj = JSON.parse( body );
                return savePokemon( bodyObj );
    
            });
        } else {
            return fetch( req );
        }


    } 
}